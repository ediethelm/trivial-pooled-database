;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :trivial-pooled-database
  :name "trivial-pooled-database"
  :description "A DB multi-threaded connection pool."
  :version "0.1.11"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-utilities
	       :trivial-object-lock
	       :log4cl
	       :bordeaux-threads
	       :iterate
	       :cl-dbi
	       :parse-number)
  :components ((:file "package")
	       (:file "trivial-pooled-database")))

